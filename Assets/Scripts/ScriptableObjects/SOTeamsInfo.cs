﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenuAttribute(fileName = "TeamsInfo.asset", menuName = "ScriptableObjects/TeamsInfo")]
public class SOTeamsInfo : ScriptableObject {
    
    public Color teamColor1;
    public Color teamColor2;
    public Color teamColor3;
    public Color teamColor4;
    public Color teamColor5;
    public Color teamColor6;
    public Color teamColor7;
    public Color teamColor8;

    public string teamName1;
    public string teamName2;
    public string teamName3;
    public string teamName4;
    public string teamName5;
    public string teamName6;
    public string teamName7;
    public string teamName8;
}
