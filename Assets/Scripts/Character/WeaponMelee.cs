﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMelee : MonoBehaviour {

    private Animator animator;
    public GameObject endPositionGO;

    [HideInInspector]
    public PlayerController Owner;

	// Use this for initialization
	void Awake () {
        animator = GetComponent<Animator>();
        Owner = GetComponentInParent<PlayerController>();
	}
	
    //ATTACK
    public void Attack() {
        animator.SetTrigger("Attack");
    }

    //ATTACK ENDED
    public void AttackEnded() {
        Owner.AttackEnded();
    }
}
