﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int OwnerPlayer = 1;
    public float speed = 4f;
    public float runSpeed = 7.5f;
    public float attackCooldown = 1f;

    private bool bIsRunning;
    private bool bAttackAlreadyDone;
    private bool bIsEnabled;
    private bool bIsInvisible;
    private bool bIsAttacking;

    private float nextAvailableAttackTime = 0f;

    public GameObject physicalParts;
    public ParticleSystem smokePS;
    public Animator dashAnimator;
    private WeaponMelee weapon;

    //EL SONIDO NO MOLA
    //public AudioSource walkAS;
    //public AudioSource runAS;
    public AudioSource attackAS;

    //public float walkSoundTime = 0.5f;
    //public float runSoundTime = 0.25f;
    //private float nextWalkSoundTime = 0f;

    private Rigidbody rigidbody;

    private Vector3 auxVector3;
    private Vector3 dashDirection;

    // Use this for initialization
    void Awake() {
        rigidbody = GetComponent<Rigidbody>();
        weapon = GetComponentInChildren<WeaponMelee>();

        bIsRunning = false;
        bAttackAlreadyDone = false;
        bIsEnabled = false;
        bIsAttacking = false;

        auxVector3 = new Vector3();
        dashDirection = new Vector3();
    }

    // Update is called once per frame
    void Update() {
        if (bIsEnabled && !bIsAttacking) {
            Move();
            CheckAttack();
        }
    }

    //MOVE
    /**/
    void Move() {
        auxVector3.Set(Input.GetAxis("Horizontal" + OwnerPlayer), 0f, Input.GetAxis("Vertical" + OwnerPlayer));

        if (auxVector3.sqrMagnitude != 0) {
            auxVector3 = Camera.main.transform.TransformDirection(auxVector3);
            auxVector3.y = 0f;
        }

        //miramos si estamos corriendo
        if (Input.GetAxis("Run" + OwnerPlayer) == 1) {
            rigidbody.velocity = (auxVector3.normalized) * runSpeed;
            if (((auxVector3.normalized) * runSpeed).sqrMagnitude > 0.5f) {
                if (!smokePS.isPlaying) {
                    smokePS.Play();
                }

                //EL SONIDO NO MOLA
                //if (Time.time >= nextWalkSoundTime) {
                //    walkAS.Play();
                //    nextWalkSoundTime = Time.time + runSoundTime;
                //}
            }
            else if (smokePS.isPlaying && ((auxVector3.normalized) * runSpeed).sqrMagnitude < 0.5f) {
                smokePS.Stop();
            }
            bIsRunning = true;
        }
        else {
            //si no estamos corriendo, nos movemos a velocidad normal
            rigidbody.velocity = (auxVector3.normalized) * speed;
            if (smokePS.isPlaying) {
                smokePS.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
            bIsRunning = false;
        }

        //Si nos estamos moviendo, rotamos el personaje y reproducimos el sonido
        if (rigidbody.velocity != Vector3.zero) {
            transform.rotation = Quaternion.LookRotation(auxVector3.normalized, Vector3.up);

            //EL SONIDO NO MOLA :(
            //if (Time.time >= nextWalkSoundTime) {
            //    walkAS.pitch = Random.Range(0.8f, 1.2f);
            //    walkAS.Play();
            //    nextWalkSoundTime = Time.time + walkSoundTime;
            //}
        }
    }

    //CHECK ATTACK
    void CheckAttack() {
        if (Time.time >= nextAvailableAttackTime) {
            if (Input.GetAxis("Melee" + OwnerPlayer) == 1) {
                if (!bAttackAlreadyDone) {
                    dashAnimator.SetTrigger("Attack");
                    attackAS.Play();
                    bIsAttacking = true;
                    bAttackAlreadyDone = true;

                    dashDirection = transform.forward;

                    rigidbody.velocity = Vector3.zero;
                }
            }
            else {
                bAttackAlreadyDone = false;
            }
        }
    }

    //ATTACK ENDED
    public void AttackEnded() {
        this.transform.position = new Vector3(weapon.endPositionGO.transform.position.x, 0.3f, weapon.endPositionGO.transform.position.z);
        bIsAttacking = false;

        nextAvailableAttackTime = Time.time + attackCooldown;
    }



    //SET IS INVISIBLE
    public void SetIsInvisible(bool Value) {
        bIsInvisible = Value;

        physicalParts.SetActive(!bIsInvisible);
    }

    //SET IS ENABLED
    public void SetIsEnabled(bool value) {
        bIsEnabled = value;
    }

    //ON TRIGGER ENTER
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.GetComponent<GrassScript>()) {
            other.gameObject.GetComponent<GrassScript>().Activate();
        }
    }

    //ON COLLISION ENTER
    private void OnCollisionEnter(Collision collision) {
        if (bIsEnabled && collision.collider.gameObject.GetComponent<WeaponMelee>() && collision.collider.gameObject.GetComponent<WeaponMelee>().Owner != this) {
            GameManager.instance.PlayerDead(OwnerPlayer);
        }
    }

}
