﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFX : MonoBehaviour {
    

    public void PlayEffect(Transform target) {
        this.transform.LookAt(target);
        this.transform.localScale = new Vector3(1, 1, Vector3.Distance(target.position, this.transform.position));

        GetComponent<Animator>().SetTrigger("Play");

    }

}
