﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionVisualizer : MonoBehaviour {

    public Color color = Color.white;
    public float radius = 0.3f;

   void OnDrawGizmosSelected() {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
