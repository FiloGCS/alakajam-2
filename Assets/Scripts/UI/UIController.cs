﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public static UIController instance;

    private PostProcessingBehaviour postProcessBehaviour;

    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject gameOverMenu;
    [SerializeField]
    private GameObject rulesMenu;
    [SerializeField]
    private GameObject pauseMenuContinueButton;
    [SerializeField]
    private GameObject gameOverMenuContinueButton;
    [SerializeField]
    private GameObject rulesMenuContinueButton;
    [SerializeField]
    private GameObject mainMenuContinueButton;
    [SerializeField]
    private Text victoryText;

    //AWAKE
    void Awake() {
        instance = this;
        postProcessBehaviour = Camera.main.GetComponent<PostProcessingBehaviour>();
    }

    //START
    void Start() {
        mainMenu.SetActive(true);
        pauseMenu.SetActive(false);
        gameOverMenu.SetActive(false);
        rulesMenu.SetActive(false);
        SetBlur(true);
    }

    //UPDATE
    private void Update() {
        if(Input.GetAxis("Exit") > 0 && (GameManager.instance.gameState == GameManager.EGameState.Playing || GameManager.instance.gameState == GameManager.EGameState.SpawnSelection)) {
            PauseMenu();
        }
    }

    //START GAME
    public void StartGame() {
        mainMenu.SetActive(false);
        SetBlur(false);
        GameManager.instance.ChangeGameState(GameManager.EGameState.SpawnSelection);
    }

    //MAIN MENU
    public void MainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    //PAUSE MENU
    void PauseMenu() {
        pauseMenu.SetActive(true);
        SetBlur(true);
        Time.timeScale = 0.01f;
        EventSystem.current.SetSelectedGameObject(pauseMenuContinueButton);
    }

    //GAME OVER MENU
    public void GameOverMenu() {
        gameOverMenu.SetActive(true);
        SetBlur(true);
        Time.timeScale = 0.1f;
        EventSystem.current.SetSelectedGameObject(gameOverMenuContinueButton);

        Color winnerColor;
        string winnerText;
        if(ScoreManager.instance.CheckWinner() == 1) {
            winnerColor = GameManager.instance.player1Color;
            winnerText = "Cyan";
        } else {
            winnerColor = GameManager.instance.player2Color;
            winnerText = "Red";
        }
        victoryText.color = winnerColor;
        victoryText.text = winnerText + " player has been choosen by the Gods!";
    }

    //CONTINUE GAME
    public void ContinueGame() {
        pauseMenu.SetActive(false);
        SetBlur(false);
        Time.timeScale = 1f;
    }

    //RULES MENU
    public void RulesMenu() {
        mainMenu.SetActive(false);
        rulesMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(rulesMenuContinueButton);
    }

    //EXIT RULES
    public void ExitRules() {
        rulesMenu.SetActive(false);
        mainMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(mainMenuContinueButton);
    }

    //SET BLUR
    private void SetBlur(bool value) {
        if (postProcessBehaviour) {
            postProcessBehaviour.profile.depthOfField.enabled = value;
        }
    }
}
