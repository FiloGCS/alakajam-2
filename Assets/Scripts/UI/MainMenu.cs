﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    
    //PLAY BUTTON
    public void PlayButton() {
        UIController.instance.StartGame();
    }

    //RULES BUTTON
    public void RulesButton() {

    }


    //EXIT BUTTON
    public void ExitButton() {
        Application.Quit();
    }
}
