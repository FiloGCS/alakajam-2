﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance;

    public ScoreTotem player1ScoreTotem;
    public ScoreTotem player2ScoreTotem;

    private int player1Score = 0;
    private int player2Score = 0;

    private int maxScore = 4;

    private void Awake() {
        instance = this;
    }

    //ADD SCORE
    public void AddScore(int player) {
        switch (player) {
            case 1:
                player1Score++;
                player1ScoreTotem.SpawnOrb(player1Score);
                break;
            case 2:
                player2Score++;
                player2ScoreTotem.SpawnOrb(player2Score);
                break;
        }
    }

    //CHECK WINNER
    public int CheckWinner() {
        if(player1Score == maxScore) {
            return 1;
        } else if(player2Score == maxScore) {
            return 2;
        }

        return 0;
    }


}
