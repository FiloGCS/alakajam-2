﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTotem : MonoBehaviour {

    public GameObject scoreOrbGO;
    public int ownerPlayer;

    //SPAWN ORB
    public void SpawnOrb(int currentScore) {
        Instantiate(scoreOrbGO, transform.GetChild(0).GetChild(currentScore - 1));
    }

    //PLAY SPAWN EFFECT
    public void PlaySpawnEffect(Transform spawnPosition) {
        SpawnFX[] spawnFXs = GetComponentsInChildren<SpawnFX>();
        List<SpawnPoint> sp = SpawnManager.instance.GetSpawnPoints(ownerPlayer);
        spawnFXs[0].PlayEffect(sp[0].transform);
        spawnFXs[1].PlayEffect(sp[1].transform);
        spawnFXs[2].PlayEffect(sp[2].transform);

    }

}
