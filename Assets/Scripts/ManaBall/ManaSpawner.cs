﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaSpawner : MonoBehaviour {

    public static ManaSpawner instance;

    public GameObject manaBallGO;
    public float minTimeBetweenSpawns;
    public float maxTimeBetweenSpawns;

    private float nextTimeSpawn;
    private bool bIsActive;

    Transform[] spawnPoints;

    private void Awake() {
        instance = this;

        spawnPoints = new Transform[transform.childCount];
        for(int i = 0; i < transform.childCount; i++) {
            spawnPoints[i] = transform.GetChild(i);
        }

        bIsActive = false;
    }
    
	
	// Update is called once per frame
	void Update () {
		if(bIsActive && Time.time >= nextTimeSpawn) {
            SpawnManaBall();
            nextTimeSpawn = Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns) + Time.time;
        }
	}

    //SPAWN MANA BALL
    void SpawnManaBall() {
        Vector3 position = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
        Instantiate(manaBallGO, position, Quaternion.identity);
    }

    //SET ACTIVE
    public void SetActive(bool value) {
        bIsActive = value;

        if(bIsActive) {
            nextTimeSpawn = Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns) + Time.time;
        }
    }
}
