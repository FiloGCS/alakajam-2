﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaBall : MonoBehaviour {

    private int ownerPlayer = 0;

    public float startSpeed = 1f;
    public float maxSpeed = 5f;
    public float accelleration = 0.05f;
    public float maxRotation = 10f;
    public float distanceToStartDecelleration = 3f;
    public float decellerationForce = 0.5f;

    private Vector3 destinationPosition;
    private bool bIsMoving = false;
    private float currentSpeed;
    private float sqrDistanceToStartDecelleration;
    private bool bIsDecellerating = false;


    private void Start() {
        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 180f);
        sqrDistanceToStartDecelleration = Mathf.Pow(distanceToStartDecelleration, 2);
    }


    private void Update() {
        if(bIsMoving) {
            Move();
        }
    }

    //MOVE
    void Move() {
        if ((transform.position - destinationPosition).sqrMagnitude <= 0.25f) {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            Mask.instance.ActivateMask(ownerPlayer, this);
            bIsMoving = false;
        } else if((transform.position - destinationPosition).sqrMagnitude <= distanceToStartDecelleration) {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.position = Vector3.Lerp(transform.position, destinationPosition, decellerationForce);
        } else {
            if(currentSpeed < maxSpeed) {
                currentSpeed += accelleration * Time.deltaTime;
                if(currentSpeed > maxSpeed) {
                    currentSpeed = maxSpeed;
                }
            }

            GetComponent<Rigidbody>().rotation = Quaternion.RotateTowards(GetComponent<Rigidbody>().rotation, Quaternion.LookRotation(destinationPosition - transform.position, Vector3.up), maxRotation);
            GetComponent<Rigidbody>().velocity = transform.forward * currentSpeed;
        }
    }

    //ON TRIGGER ENTER
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.GetComponent<PlayerController>()) {
            currentSpeed = startSpeed;
            bIsMoving = true;
            ownerPlayer = other.gameObject.GetComponent<PlayerController>().OwnerPlayer;
            destinationPosition = Mask.instance.GetPlayerTransform(ownerPlayer).position;

        }
    }
}
