﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public static SpawnManager instance;

    SpawnPoint[] spawnPoints;

    // Use this for initialization
    void Awake() {
        instance = this;

        spawnPoints = new SpawnPoint[transform.childCount];

        for (int i = 0; i < transform.childCount; i++) {
            spawnPoints[i] = transform.GetChild(i).GetComponent<SpawnPoint>();
        }
    }


    //SETUP SPAWN SELECTION
    public void SetupSpawnSelection() {
        int[] assignedPositionsAmount = new int[] { 0, 0 }; //En cada posicion se guarda la cantidad de veces que se ha asigando un punto al jugador de la misma posicion
        List<int> playersAvailables = new List<int> { 1, 2 };
        List<int> spawnPointsAvailables = new List<int>();
        int maxPositionsPerPlayer = 3;
        int auxPlayer;
        int auxSpawnPoint;

        for (int i = 0; i < transform.childCount; i++) {
            spawnPointsAvailables.Add(i);
        }

        for (int i = 0; i < spawnPoints.Length; i++) {

            //Escogemos un jugador y un SpawnPoint aleatorio de entre los disponibles
            auxPlayer = playersAvailables[Random.Range(0, playersAvailables.Count)];
            auxSpawnPoint = spawnPointsAvailables[Random.Range(0, spawnPointsAvailables.Count)];

            //Configuramos el SpawnPoint seleccionado
            spawnPoints[auxSpawnPoint].SetupSpawnPoint((SpawnPoint.ESpanwPointButtonID)assignedPositionsAmount[auxPlayer - 1], auxPlayer);

            assignedPositionsAmount[auxPlayer - 1]++;

            //Si ya se han asignado todas las posiciones para un jugador, lo eliminamos
            if (assignedPositionsAmount[auxPlayer - 1] >= maxPositionsPerPlayer) {
                playersAvailables.Remove(auxPlayer);
            }

            //Eliminamos de la lista el SpawnPoint que acabamos de configurar
            spawnPointsAvailables.Remove(auxSpawnPoint);
        }
    }

    //GET SPAWN POINT
    public SpawnPoint GetSpawnPoint(int player, SpawnPoint.ESpanwPointButtonID buttonID) {
        foreach (SpawnPoint sp in spawnPoints) {
            if (sp.currentPlayer == player && sp.currentButtonID == buttonID) {
                return sp;
            }
        }

        return null;
    }

    //GET SPAWN POINT
    public List<SpawnPoint> GetSpawnPoints(int player) {
        List<SpawnPoint> result = new List<SpawnPoint>();
        foreach (SpawnPoint sp in spawnPoints) {
            if (sp.currentPlayer == player) {
                result.Add(sp);
            }
        }

        return result;
    }

    //HIDE SPAWN POINTS
    public void HideSpawnPoints(int player) {
        foreach (SpawnPoint sp in spawnPoints) {
            if (player == 0) {
                sp.ShowUI(false);
            } else if(sp.currentPlayer == player) {
                sp.ShowUI(false);
            }
        }
    }
}
