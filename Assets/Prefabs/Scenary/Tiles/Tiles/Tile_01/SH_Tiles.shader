// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Tile Material"
{
	Properties
	{
		_baldosa_01_low_None_BaseColor("baldosa_01_low_None_BaseColor", 2D) = "white" {}
		_baldosa_01_low_None_Normal("baldosa_01_low_None_Normal", 2D) = "bump" {}
		_baldosa_01_low_None_Roughness("baldosa_01_low_None_Roughness", 2D) = "white" {}
		_Texture0("Texture 0", 2D) = "white" {}
		_RandomTintB("Random Tint B", Color) = (0,0,0,0)
		_RandomTintA("Random Tint A", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _baldosa_01_low_None_Normal;
		uniform float4 _baldosa_01_low_None_Normal_ST;
		uniform float4 _RandomTintA;
		uniform sampler2D _baldosa_01_low_None_BaseColor;
		uniform float4 _baldosa_01_low_None_BaseColor_ST;
		uniform float4 _RandomTintB;
		uniform sampler2D _Texture0;
		uniform sampler2D _baldosa_01_low_None_Roughness;
		uniform float4 _baldosa_01_low_None_Roughness_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_baldosa_01_low_None_Normal = i.uv_texcoord * _baldosa_01_low_None_Normal_ST.xy + _baldosa_01_low_None_Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _baldosa_01_low_None_Normal, uv_baldosa_01_low_None_Normal ) );
			float2 uv_baldosa_01_low_None_BaseColor = i.uv_texcoord * _baldosa_01_low_None_BaseColor_ST.xy + _baldosa_01_low_None_BaseColor_ST.zw;
			float4 tex2DNode1 = tex2D( _baldosa_01_low_None_BaseColor, uv_baldosa_01_low_None_BaseColor );
			float4 transform5 = mul(unity_WorldToObject,float4( 0,0,0,1 ));
			float2 appendResult6 = (float2(transform5.x , transform5.z));
			float4 lerpResult12 = lerp( ( _RandomTintA * tex2DNode1 ) , ( tex2DNode1 * _RandomTintB ) , ( tex2D( _Texture0, appendResult6 ).r * 1.5 ));
			o.Albedo = lerpResult12.rgb;
			float2 uv_baldosa_01_low_None_Roughness = i.uv_texcoord * _baldosa_01_low_None_Roughness_ST.xy + _baldosa_01_low_None_Roughness_ST.zw;
			o.Smoothness = ( 1.0 - tex2D( _baldosa_01_low_None_Roughness, uv_baldosa_01_low_None_Roughness ) ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;1236.851;1057.044;1;True;True
Node;AmplifyShaderEditor.WorldToObjectTransfNode;5;-935,-751;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;6;-729,-713;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;7;-741,-911;Float;True;Property;_Texture0;Texture 0;3;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;False;white;Auto;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;8;-484,-752;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;13;-546,-558;Float;False;Property;_RandomTintA;Random Tint A;5;0;Create;True;0;0,0,0,0;0.375,1,0.4310343,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-666,-373;Float;True;Property;_baldosa_01_low_None_BaseColor;baldosa_01_low_None_BaseColor;0;0;Create;True;0;355590c1b200db64183f3eaf8d4688cf;e0663d3f5c1736c4ba88131bc59c0e4f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;14;-562,-192;Float;False;Property;_RandomTintB;Random Tint B;4;0;Create;True;0;0,0,0,0;1,0.9117647,0.968357,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-184.8513,-748.0436;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-235,-481;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-220,-180;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-596,138;Float;True;Property;_baldosa_01_low_None_Roughness;baldosa_01_low_None_Roughness;2;0;Create;True;0;9c1c9c512a8de144fbdfad7a8581f217;7ec055e2de2a61c4ab7c53323bcd399f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;12;28,-446;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;2;-569,-53;Float;True;Property;_baldosa_01_low_None_Normal;baldosa_01_low_None_Normal;1;0;Create;True;0;3aab3b24d51faaa4c9daaa341232451f;e6f5a6a2b018bb44698f2fc64972e2ee;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;4;-229,138;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2,-2;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Tile Material;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;5;1
WireConnection;6;1;5;3
WireConnection;8;0;7;0
WireConnection;8;1;6;0
WireConnection;17;0;8;1
WireConnection;16;0;13;0
WireConnection;16;1;1;0
WireConnection;15;0;1;0
WireConnection;15;1;14;0
WireConnection;12;0;16;0
WireConnection;12;1;15;0
WireConnection;12;2;17;0
WireConnection;4;0;3;0
WireConnection;0;0;12;0
WireConnection;0;1;2;0
WireConnection;0;4;4;0
ASEEND*/
//CHKSM=872B699D6E9908BB9216C0A8463F8DF262F49C20