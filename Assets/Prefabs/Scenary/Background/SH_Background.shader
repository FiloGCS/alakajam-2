// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Fondo"
{
	Properties
	{
		_Clouds("Clouds", 2D) = "white" {}
		_WarpIntensity("Warp Intensity", Range( 0 , 0.1)) = 0
		_NoiseScale("Noise Scale", Range( 0 , 10)) = 0
		_Speed("Speed", Range( 0 , 2)) = 0
		_BuildingsMask("Buildings Mask", 2D) = "white" {}
		_Buildings("Buildings", 2D) = "white" {}
		_Warp("Warp", 2D) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float4 screenPos;
		};

		uniform sampler2D _Clouds;
		uniform sampler2D _Warp;
		uniform float _Speed;
		uniform float _NoiseScale;
		uniform float _WarpIntensity;
		uniform sampler2D _Buildings;
		uniform sampler2D _BuildingsMask;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 appendResult2 = (float2(ase_screenPosNorm.x , ase_screenPosNorm.y));
			float2 temp_output_29_0 = ( appendResult2 * _NoiseScale );
			float2 panner18 = ( temp_output_29_0 + 1 * _Time.y * ( float2( 1,-1 ) * _Speed ));
			float4 tex2DNode17 = tex2D( _Warp, panner18 );
			float2 appendResult23 = (float2(tex2DNode17.r , tex2DNode17.g));
			float2 panner19 = ( temp_output_29_0 + 1 * _Time.y * ( _Speed * float2( 0.5,-0.25 ) ));
			float4 tex2DNode20 = tex2D( _Warp, panner19 );
			float2 appendResult22 = (float2(tex2DNode20.g , tex2DNode20.b));
			float2 appendResult40 = (float2(ase_screenPosNorm.x , ase_screenPosNorm.y));
			float4 lerpResult38 = lerp( tex2D( _Clouds, ( appendResult2 + ( ( appendResult23 * appendResult22 ) * _WarpIntensity ) ) ) , tex2D( _Buildings, appendResult40 ) , tex2D( _BuildingsMask, appendResult40 ).r);
			o.Emission = lerpResult38.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;1688.289;476.3393;1;True;True
Node;AmplifyShaderEditor.ScreenPosInputsNode;1;-1759,-280;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;33;-1405.134,773.04;Float;False;Constant;_Vector1;Vector 1;6;0;Create;True;0;0.5,-0.25;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;32;-1533.134,299.04;Float;True;Constant;_Vector0;Vector 0;6;0;Create;True;0;1,-1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;31;-1589.134,574.04;Float;False;Property;_Speed;Speed;3;0;Create;True;0;0;0.12;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1486.378,87.40396;Float;False;Property;_NoiseScale;Noise Scale;2;0;Create;True;0;0;10;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;2;-1523,-280;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-1087.134,629.04;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-1159.378,-10.59604;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-1174.134,347.04;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;18;-901.726,20.07593;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.5,-0.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;19;-900.726,240.0759;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.4,0.2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;41;-932.2886,-191.3393;Float;True;Property;_Warp;Warp;6;0;Create;True;0;None;275e3ab0ff482a1488ab55465aef9497;False;white;Auto;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;17;-607.726,6.075928;Float;True;Property;_noise;noise;1;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;20;-594.726,247.0759;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;23;-314.726,15.07593;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;22;-298.726,251.0759;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-21.72595,118.0759;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;27;-8.960815,335.021;Float;False;Property;_WarpIntensity;Warp Intensity;1;0;Create;True;0;0;0.1;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;278.0392,123.021;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;39;172.8693,514.2965;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;28;341.0392,-160.979;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;40;408.8693,514.2965;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;37;510.0443,-52.14694;Float;True;Property;_Buildings;Buildings;5;0;Create;True;0;fa3ffadd1a5c6cf45a93da3dde6b7eb0;89f35de2416ad3f4a9775081c07a4ca4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5;602.4927,-308.9037;Float;True;Property;_Clouds;Clouds;0;0;Create;True;0;efa5354039676fc4eb6d9fbe248fb03d;2a4e56f97dfc12c4488ca127633523e6;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;36;646.902,243.3159;Float;True;Property;_BuildingsMask;Buildings Mask;4;0;Create;True;0;c586f93353e936147a679fdc8ee81318;de1118b24b35f404c898076c804f1bc3;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;38;876.6127,30.09619;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1090.992,-104.6038;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Fondo;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;1;1
WireConnection;2;1;1;2
WireConnection;35;0;31;0
WireConnection;35;1;33;0
WireConnection;29;0;2;0
WireConnection;29;1;30;0
WireConnection;34;0;32;0
WireConnection;34;1;31;0
WireConnection;18;0;29;0
WireConnection;18;2;34;0
WireConnection;19;0;29;0
WireConnection;19;2;35;0
WireConnection;17;0;41;0
WireConnection;17;1;18;0
WireConnection;20;0;41;0
WireConnection;20;1;19;0
WireConnection;23;0;17;1
WireConnection;23;1;17;2
WireConnection;22;0;20;2
WireConnection;22;1;20;3
WireConnection;21;0;23;0
WireConnection;21;1;22;0
WireConnection;26;0;21;0
WireConnection;26;1;27;0
WireConnection;28;0;2;0
WireConnection;28;1;26;0
WireConnection;40;0;39;1
WireConnection;40;1;39;2
WireConnection;37;1;40;0
WireConnection;5;1;28;0
WireConnection;36;1;40;0
WireConnection;38;0;5;0
WireConnection;38;1;37;0
WireConnection;38;2;36;1
WireConnection;0;2;38;0
ASEEND*/
//CHKSM=A9E09E2E04CD821129AEEB405BFF6A6A9245FF70