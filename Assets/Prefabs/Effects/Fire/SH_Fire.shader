// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Filo/Fire"
{
	Properties
	{
		_WarpIntensity("Warp Intensity", Range( 0 , 1)) = 0.5399377
		_InnerColor("Inner Color", Color) = (1,0.6827587,0,0)
		_OuterColor("Outer Color", Color) = (1,0,0,0)
		_EmissiveIntensity("Emissive Intensity", Range( 1 , 5)) = 1.69
		_FireTexture("Fire Texture", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _OuterColor;
		uniform float4 _InnerColor;
		uniform sampler2D _FireTexture;
		uniform float _WarpIntensity;
		uniform float4 _FireTexture_ST;
		uniform float _EmissiveIntensity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TexCoord9 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float4 transform30 = mul(unity_WorldToObject,float4( 0,0,0,1 ));
			float2 uv_TexCoord6 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 panner3 = ( ( transform30.x + uv_TexCoord6 ) + 1 * _Time.y * float2( 0,-1.2 ));
			float4 transform46 = mul(unity_WorldToObject,float4( 0,0,0,1 ));
			float2 uv_TexCoord45 = i.uv_texcoord * float2( 2,2 ) + float2( 0.5,0.5 );
			float2 panner4 = ( ( transform46.x + uv_TexCoord45 ) + 1 * _Time.y * float2( 0,-1.5 ));
			float2 appendResult18 = (float2(uv_TexCoord9.x , ( ( ( _WarpIntensity * ( tex2D( _FireTexture, panner3 ).b * tex2D( _FireTexture, panner4 ).b ) ) * -1 ) + uv_TexCoord9.y )));
			float2 uv_FireTexture = i.uv_texcoord * _FireTexture_ST.xy + _FireTexture_ST.zw;
			float clampResult42 = clamp( ( tex2D( _FireTexture, appendResult18 ).r + tex2D( _FireTexture, uv_FireTexture ).g ) , 0 , 1 );
			float4 lerpResult23 = lerp( _OuterColor , _InnerColor , clampResult42);
			o.Emission = ( lerpResult23 * _EmissiveIntensity ).rgb;
			o.Alpha = clampResult42;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1911;45;1906;1004;2293.967;682.6461;1.400622;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;45;-1997.719,121.9692;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0.5,0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldToObjectTransfNode;30;-2039.546,-535.1971;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-2051.119,-311.5658;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldToObjectTransfNode;46;-2005.972,-97.1273;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;47;-1718.711,24.08627;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-1752.286,-413.9836;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;50;-1626.084,289.3637;Float;True;Property;_FireTexture;Fire Texture;4;0;Create;True;0;None;69a7070dc331fb84baaae31705eb26db;False;white;Auto;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.PannerNode;4;-1581.112,7.842437;Float;False;3;0;FLOAT2;1,1;False;2;FLOAT2;0,-1.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;3;-1597.457,-421.3007;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1.2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;43;-1386.871,-466.8632;Float;True;Property;_T_Fire_all;T_Fire_all;8;0;Create;True;0;69a7070dc331fb84baaae31705eb26db;69a7070dc331fb84baaae31705eb26db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;44;-1378.067,-36.51987;Float;True;Property;_TextureSample1;Texture Sample 1;8;0;Create;True;0;69a7070dc331fb84baaae31705eb26db;69a7070dc331fb84baaae31705eb26db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1023.937,-230.1579;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-1044.601,-418.5726;Float;False;Property;_WarpIntensity;Warp Intensity;0;0;Create;True;0;0.5399377;0.4141247;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-768.275,-397.1476;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-557.5967,-466.8076;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-799.7394,-2.572502;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-528.7393,-175.5923;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-378.5232,22.26467;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;49;-197.102,195.7107;Float;True;Property;_TextureSample2;Texture Sample 2;8;0;Create;True;0;69a7070dc331fb84baaae31705eb26db;69a7070dc331fb84baaae31705eb26db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;48;-197.102,-5.703059;Float;True;Property;_TextureSample0;Texture Sample 0;8;0;Create;True;0;69a7070dc331fb84baaae31705eb26db;69a7070dc331fb84baaae31705eb26db;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;41;152.2143,123.9255;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;42;368.6199,123.2298;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;20;388.4944,-138.0808;Float;False;Property;_InnerColor;Inner Color;1;0;Create;True;0;1,0.6827587,0,0;1,0.755071,0.2279412,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;386.0327,-312.4373;Float;False;Property;_OuterColor;Outer Color;2;0;Create;True;0;1,0,0,0;1,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;22;681.504,-341.7288;Float;False;Property;_EmissiveIntensity;Emissive Intensity;3;0;Create;True;0;1.69;2.33;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;23;689.2512,-144.497;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;980.1339,-167.1279;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1194.195,-14.70983;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Filo/Fire;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Back;0;0;False;0;0;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;47;0;46;1
WireConnection;47;1;45;0
WireConnection;32;0;30;1
WireConnection;32;1;6;0
WireConnection;4;0;47;0
WireConnection;3;0;32;0
WireConnection;43;0;50;0
WireConnection;43;1;3;0
WireConnection;44;0;50;0
WireConnection;44;1;4;0
WireConnection;25;0;43;3
WireConnection;25;1;44;3
WireConnection;15;0;16;0
WireConnection;15;1;25;0
WireConnection;26;0;15;0
WireConnection;10;0;26;0
WireConnection;10;1;9;2
WireConnection;18;0;9;1
WireConnection;18;1;10;0
WireConnection;49;0;50;0
WireConnection;48;0;50;0
WireConnection;48;1;18;0
WireConnection;41;0;48;1
WireConnection;41;1;49;2
WireConnection;42;0;41;0
WireConnection;23;0;24;0
WireConnection;23;1;20;0
WireConnection;23;2;42;0
WireConnection;21;0;23;0
WireConnection;21;1;22;0
WireConnection;0;2;21;0
WireConnection;0;9;42;0
ASEEND*/
//CHKSM=43BA4A7754A6B1EEC73FF2DBB670E340C92228BA