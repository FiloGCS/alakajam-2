// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "CinturonMana"
{
	Properties
	{
		_Background("Background", 2D) = "white" {}
		_noise("noise", 2D) = "white" {}
		_WarpIntensity("Warp Intensity", Range( 0 , 1)) = 0
		_Fader("Fader", 2D) = "white" {}
		_Float2("Float 2", Range( 0.5 , 2)) = 1.494118
		_EmissiveIntensity("Emissive Intensity", Range( 1 , 5)) = 1.53
		_Color0("Color 0", Color) = (1,0.4344828,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float _EmissiveIntensity;
		uniform sampler2D _Fader;
		uniform float _Float2;
		uniform sampler2D _Background;
		uniform float4 _Background_ST;
		uniform float _WarpIntensity;
		uniform sampler2D _noise;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float mulTime25 = _Time.y * 0.6;
			float2 uv_TexCoord24 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 panner23 = ( uv_TexCoord24 + mulTime25 * float2( 0.5,0.7 ));
			v.vertex.xyz += ( _WarpIntensity * tex2Dlod( _noise, float4( panner23, 0, 0) ) ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Emission = ( _Color0 * _EmissiveIntensity ).rgb;
			float mulTime16 = _Time.y * 0.7;
			float mulTime17 = _Time.y * 0.2;
			float2 appendResult8 = (float2(mulTime16 , mulTime17));
			float2 uv_TexCoord7 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 uv_Background = i.uv_texcoord * _Background_ST.xy + _Background_ST.zw;
			float4 clampResult18 = clamp( ( ( tex2D( _Fader, ( appendResult8 + uv_TexCoord7 ) ) * _Float2 ) - tex2D( _Background, uv_Background ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Alpha = clampResult18.r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;2127.076;409.4312;1.703085;True;True
Node;AmplifyShaderEditor.SimpleTimeNode;16;-1302.683,268.0959;Float;False;1;0;FLOAT;0.7;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;17;-1341.132,500.3091;Float;False;1;0;FLOAT;0.2;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;8;-1040.169,390.7256;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-1124.169,521.7256;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-844.1699,454.7255;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;25;-1406.453,-76.56068;Float;False;1;0;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;24;-1438.453,-268.5605;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;10;-697.7643,420.868;Float;True;Property;_Fader;Fader;4;0;Create;True;0;d42394d4762dc544e973583f444dba7d;0bfe8fece4c6f2041b61b22c1c5e9769;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-688.6263,624.0775;Float;False;Property;_Float2;Float 2;5;0;Create;True;0;1.494118;0;0.5;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;23;-1214.453,-124.5608;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.5,0.7;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-369.6261,532.0773;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;12;-495.4995,712.6231;Float;True;Property;_Background;Background;0;0;Create;True;0;0e49c8e98c1125c47b2f9a6cc5c09842;0e49c8e98c1125c47b2f9a6cc5c09842;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;20;44.17134,15.70937;Float;False;Property;_Color0;Color 0;7;0;Create;True;0;1,0.4344828,0,0;1,0.4344828,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;22;23.77136,185.4094;Float;False;Property;_EmissiveIntensity;Emissive Intensity;6;0;Create;True;0;1.53;1.53;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;14;-172.462,638.2487;Float;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-990.4512,-92.56078;Float;False;Property;_WarpIntensity;Warp Intensity;3;0;Create;True;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-1038.452,19.43935;Float;True;Property;_noise;noise;1;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-404.4519,46.43934;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;18;57.67104,532.8088;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1338.168,363.7256;Float;False;Property;_Float0;Float 0;2;0;Create;True;0;-0.4588235;-0.2;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;329.3713,34.90938;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;372.5997,244.7002;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;CinturonMana;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Off;0;0;False;0;0;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;8;0;16;0
WireConnection;8;1;17;0
WireConnection;9;0;8;0
WireConnection;9;1;7;0
WireConnection;10;1;9;0
WireConnection;23;0;24;0
WireConnection;23;1;25;0
WireConnection;13;0;10;0
WireConnection;13;1;11;0
WireConnection;14;0;13;0
WireConnection;14;1;12;0
WireConnection;1;1;23;0
WireConnection;2;0;3;0
WireConnection;2;1;1;0
WireConnection;18;0;14;0
WireConnection;21;0;20;0
WireConnection;21;1;22;0
WireConnection;0;2;21;0
WireConnection;0;9;18;0
WireConnection;0;11;2;0
ASEEND*/
//CHKSM=E2C7679D504175D49023C17612B276E30B4ABD53