// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Unlit/Selector"
{
	Properties
	{
		_Background("Background", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Float0("Float 0", Range( 0 , 0.1)) = 0
		_Float1("Float 1", Range( 1 , 5)) = 1.53
		_Color0("Color 0", Color) = (1,0.4344828,0,0)
		_Fader("Fader", 2D) = "white" {}
		_Float2("Float 2", Range( 0.5 , 2)) = 1.511765
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float _Float1;
		uniform sampler2D _Fader;
		uniform float _Float2;
		uniform sampler2D _Background;
		uniform float _Float0;
		uniform sampler2D _TextureSample0;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float mulTime1 = _Time.y * 0.6;
			float2 uv_TexCoord2 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 panner3 = ( uv_TexCoord2 + mulTime1 * float2( 0.2,-0.7 ));
			float2 uv_TexCoord8 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			v.vertex.xyz += ( ( _Float0 * ( tex2Dlod( _TextureSample0, float4( panner3, 0, 0) ) - float4( 0.5019608,0.5019608,0.5019608,0 ) ) ) * uv_TexCoord8.y ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Emission = ( _Color0 * _Float1 ).rgb;
			float2 uv_TexCoord28 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float mulTime14 = _Time.y * 0;
			float mulTime13 = _Time.y * -1;
			float2 appendResult15 = (float2(mulTime14 , mulTime13));
			float2 uv_TexCoord16 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float4 clampResult24 = clamp( ( ( tex2D( _Fader, ( uv_TexCoord28 - float2( 0,-0.1 ) ) ) * _Float2 ) - tex2D( _Background, ( appendResult15 + uv_TexCoord16 ) ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Alpha = clampResult24.r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1913;29;1906;1004;1067.647;497.8462;1;True;True
Node;AmplifyShaderEditor.SimpleTimeNode;14;-1540.546,354.1136;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;13;-1578.995,586.3268;Float;False;1;0;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1135.333,-104.8331;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;1;-1075.333,18.16669;Float;False;1;0;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;30;-1312.583,256.3075;Float;False;Constant;_Vector0;Vector 0;8;0;Create;True;0;0,-0.1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1540.583,108.3075;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;16;-1368.032,609.7433;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;3;-879.3335,-57.8334;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,-0.7;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;29;-1120.583,198.3075;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;15;-1278.032,476.7433;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-1082.033,540.7432;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-918.4896,414.0952;Float;False;Property;_Float2;Float 2;7;0;Create;True;0;1.511765;0;0.5;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-927.6277,210.8857;Float;True;Property;_Fader;Fader;6;0;Create;True;0;fad239a4e61f13747b0252e1fd61a088;0bfe8fece4c6f2041b61b22c1c5e9769;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-678.3333,-84.83328;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-599.4894,322.095;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;21;-725.3629,503.9409;Float;True;Property;_Background;Background;0;0;Create;True;0;0e49c8e98c1125c47b2f9a6cc5c09842;0e49c8e98c1125c47b2f9a6cc5c09842;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;9;-385,-78;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0.5019608,0.5019608,0.5019608,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-513.3333,-165.8334;Float;False;Property;_Float0;Float 0;2;0;Create;True;0;0;0;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-262.2667,-467.9667;Float;False;Property;_Color0;Color 0;5;0;Create;True;0;1,0.4344828,0,0;1,0.4344828,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-282.6666,-298.2667;Float;False;Property;_Float1;Float 1;4;0;Create;True;0;1.53;1.53;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-222.3331,-99.8333;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;22;-401.0253,428.2665;Float;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;8;-276,76;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;22.93332,-448.7667;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;24;-157.1923,369.8265;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;53,23;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-1576.031,449.7433;Float;False;Property;_Float3;Float 3;3;0;Create;True;0;-0.4588235;-0.2;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;220,-253;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Unlit/Selector;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Off;0;0;False;0;0;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;15;0;14;0
WireConnection;15;1;13;0
WireConnection;17;0;15;0
WireConnection;17;1;16;0
WireConnection;18;1;29;0
WireConnection;4;1;3;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;21;1;17;0
WireConnection;9;0;4;0
WireConnection;6;0;5;0
WireConnection;6;1;9;0
WireConnection;22;0;20;0
WireConnection;22;1;21;0
WireConnection;12;0;10;0
WireConnection;12;1;11;0
WireConnection;24;0;22;0
WireConnection;7;0;6;0
WireConnection;7;1;8;2
WireConnection;0;2;12;0
WireConnection;0;9;24;0
WireConnection;0;11;7;0
ASEEND*/
//CHKSM=9DA574D01303B51D7D1FC007C2C53D8519ABBA78