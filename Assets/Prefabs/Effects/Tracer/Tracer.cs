﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Revelador : MonoBehaviour {

    public Transform target;


	
	// Update is called once per frame
	void Update () {
        if (target) {
            this.transform.LookAt(target);
            this.transform.localScale = new Vector3(1, 1, Vector3.Distance(target.position, this.transform.position));
        }
	}
}
