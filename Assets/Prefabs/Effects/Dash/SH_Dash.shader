// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FiloVFX/Dash"
{
	Properties
	{
		_Background("Background", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_WarpStrength("Warp Strength", Range( 0 , 0.1)) = 0
		_EmissiveStrength("Emissive Strength", Range( 1 , 5)) = 1.53
		_Color0("Color 0", Color) = (1,0.4344828,0,0)
		_Fader("Fader", 2D) = "white" {}
		_FaderStrength("Fader Strength", Range( 0 , 2)) = 1.511765
		_t("t", Range( -1 , 1)) = -0.6588235
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float _EmissiveStrength;
		uniform sampler2D _Fader;
		uniform float _t;
		uniform float _FaderStrength;
		uniform sampler2D _Background;
		uniform float _WarpStrength;
		uniform sampler2D _TextureSample0;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float mulTime1 = _Time.y * 0.6;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float2 appendResult33 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 panner3 = ( ( 0.3 * appendResult33 ) + mulTime1 * float2( 0.2,-0.7 ));
			float2 uv_TexCoord8 = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			v.vertex.xyz += ( ( _WarpStrength * ( tex2Dlod( _TextureSample0, float4( panner3, 0, 0) ) - float4( 0.5019608,0.5019608,0.5019608,0 ) ) ) * uv_TexCoord8.y ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Emission = ( _Color0 * _EmissiveStrength ).rgb;
			float2 uv_TexCoord28 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float2 appendResult37 = (float2(0 , _t));
			float mulTime14 = _Time.y * 0;
			float mulTime13 = _Time.y * -1;
			float2 appendResult15 = (float2(mulTime14 , mulTime13));
			float2 uv_TexCoord16 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float4 clampResult24 = clamp( ( ( tex2D( _Fader, ( uv_TexCoord28 - appendResult37 ) ) * _FaderStrength ) - tex2D( _Background, ( appendResult15 + uv_TexCoord16 ) ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Alpha = clampResult24.r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
-1264;29;1257;1004;1862.76;395.5333;1;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;32;-1694.916,384.3799;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;38;-1506.76,-230.5333;Float;False;Property;_t;t;7;0;Create;True;0;-0.6588235;-0.14;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;33;-1497.916,395.3799;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-1516.916,305.3799;Float;False;Constant;_Float0;Float 0;7;0;Create;True;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;1;-1276.729,495.2805;Float;False;1;0;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;37;-1207.76,-224.5333;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;13;-1392.079,35.02019;Float;False;1;0;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-1315.916,328.3799;Float;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1456.667,-375.9998;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;14;-1380.63,-36.19263;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;16;-1284.118,125.4367;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;3;-1080.729,419.2803;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.2,-0.7;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;29;-1036.669,-285.9998;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;15;-1194.117,-7.563015;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;18;-843.714,-273.4216;Float;True;Property;_Fader;Fader;5;0;Create;True;0;265c0c0501f582c4cbf0a1d8a827b127;265c0c0501f582c4cbf0a1d8a827b127;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-998.119,56.43667;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;4;-879.7287,392.2805;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;275e3ab0ff482a1488ab55465aef9497;275e3ab0ff482a1488ab55465aef9497;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-833.5758,-70.21089;Float;False;Property;_FaderStrength;Fader Strength;6;0;Create;True;0;1.511765;1.808;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-714.7287,311.2804;Float;False;Property;_WarpStrength;Warp Strength;2;0;Create;True;0;0;0.0037;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-515.5753,-162.2114;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;9;-586.3951,399.1138;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0.5019608,0.5019608,0.5019608,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;21;-641.4489,19.63453;Float;True;Property;_Background;Background;0;0;Create;True;0;0e49c8e98c1125c47b2f9a6cc5c09842;0e49c8e98c1125c47b2f9a6cc5c09842;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;8;-422.3945,512.114;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-282.6666,-298.2667;Float;False;Property;_EmissiveStrength;Emissive Strength;3;0;Create;True;0;1.53;4.73;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;22;-317.1113,-56.03965;Float;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-423.7277,377.2805;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;10;-262.2667,-467.9667;Float;False;Property;_Color0;Color 0;4;0;Create;True;0;1,0.4344828,0,0;1,0.4344828,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;22.93332,-448.7667;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-103.3948,345.1138;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;24;-73.2778,-114.4796;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1731.729,35.28067;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;220,-253;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;FiloVFX/Dash;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Off;0;0;False;0;0;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;32;1
WireConnection;33;1;32;3
WireConnection;37;1;38;0
WireConnection;34;0;36;0
WireConnection;34;1;33;0
WireConnection;3;0;34;0
WireConnection;3;1;1;0
WireConnection;29;0;28;0
WireConnection;29;1;37;0
WireConnection;15;0;14;0
WireConnection;15;1;13;0
WireConnection;18;1;29;0
WireConnection;17;0;15;0
WireConnection;17;1;16;0
WireConnection;4;1;3;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;9;0;4;0
WireConnection;21;1;17;0
WireConnection;22;0;20;0
WireConnection;22;1;21;0
WireConnection;6;0;5;0
WireConnection;6;1;9;0
WireConnection;12;0;10;0
WireConnection;12;1;11;0
WireConnection;7;0;6;0
WireConnection;7;1;8;2
WireConnection;24;0;22;0
WireConnection;0;2;12;0
WireConnection;0;9;24;0
WireConnection;0;11;7;0
ASEEND*/
//CHKSM=967904099B97FC27DF509FF46BF4A3D9DFE7D3AF